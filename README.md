Requirements
-------------
Currently the module only requires these modules to be enabled.

- Field
- Image

Current Issues
---------------
- Pixel-perfect rendering is sometimes not working. To reproduce the same issue, try out the module with different number of images.
- Rendering the HTML tags to inject the images and boxes programmatically.
- Some style and effect issues, the style name's not loaded and some effects do not take place.
